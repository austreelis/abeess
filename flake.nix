{
  inputs = {
    parts = {
      url = "github:hercules-ci/flake-parts";
      inputs = { nixpkgs-lib.follows = "nci/nixpkgs"; };
    };
    nci = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs = { parts.follows = "parts"; };
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs = { nixpkgs.follows = "nci/nixpkgs"; };
    };
  };

  outputs = inputs: let
    
    inherit (inputs) devshell nci parts;

    getDefaultModule = inp: inp.flakeModules.default or inp.flakeModule;
    
  in parts.lib.mkFlake { inherit inputs; } {
    systems = [ "aarch64-linux" "x86_64-linux" ];
    imports = map getDefaultModule [ devshell nci ];
    perSystem = { lib, pkgs, config, system, ... }: let

      inherit (lib) hiPrio;
      
      setCategory = category: attrs: attrs // { inherit category; };
      mkEnvSuffix = name: suffix: "\${${name}:+$$${name}:}${suffix}";
    
      crates = config.nci.outputs;

    in {

      _module.args.pkgs = import nci.inputs.nixpkgs { inherit system; };

      nci.projects.abeess-www.relPath = "www";

      packages.default = config.packages.abeess-www;
      packages.abeess-www = crates."abeess-www".packages.release;

      devshells.default = {
        devshell.name = "abeess";
        devshell.packages = [
          pkgs.cargo-audit
          pkgs.cargo-bloat
          pkgs.cargo-expand
          pkgs.cargo-feature
          pkgs.cargo-deny
          pkgs.cargo-llvm-cov
          pkgs.clang
          pkgs.libllvm
          pkgs.lldb
          pkgs.pkg-config
        ];
        commands =
          map (setCategory "nix") [ { package = pkgs.nixpkgs-fmt; } ]
          ++ map (setCategory "rust") [ {
            package = hiPrio config.nci.toolchains.shell;
            name = "cargo";
            help = pkgs.cargo.meta.description;
          } ];
        env = [
          { name = "LIBRARY_PATH"; eval = "$DEVSHELL_DIR/lib"; }
          {
            name = "LD_LIBRARY_PATH";
            eval = mkEnvSuffix "LD_LIBRARY_PATH" "$DEVSHELL_DIR/lib";
          }
          {
            name = "C_INCLUDE_PATH";
            eval = mkEnvSuffix "C_INCLUDE_PATH" "$DEVSHELL_DIR/include";
          }
          {
            name = "PKG_CONFIG_PATH";
            eval = mkEnvSuffix "PKG_CONFIG_PATH" "$DEVSHELL_DIR/lib/pkgconfig";
          }
          { name = "CFLAGS"; eval = ''-I "$DEVSHELL_DIR/include"''; }
          {
            name = "RUST_SRC_PATH";
            eval = "$DEVSHELL_DIR/lib/rustlib/src/rust/library";
          }
        ];
      };

    };
  };
}

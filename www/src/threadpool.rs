//! TODO

use std::{
    any::{type_name, Any},
    num::NonZeroUsize,
    sync::{
        atomic::AtomicUsize,
        mpsc::{SendError, Sender},
        Arc, Mutex,
    },
    thread::{Builder, JoinHandle},
};

const INT2STR: &[&str] = &[
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
];

pub struct ThreadPool {
    workers: Vec<JoinHandle<()>>,
    job_sender: Sender<Box<dyn FnOnce() + Send + 'static>>,
    // TODO: Use better/custom channel (crossbeam ?) that allow accessing its length.
    //       std::sync::mpmc, the underlying impl of std::sync::mpsc, does, but
    //       it's not public.
    queue_len: Arc<AtomicUsize>,
    #[allow(dead_code)]
    base_name: String,
    #[allow(dead_code)]
    default_stack_size: Option<NonZeroUsize>,
}

impl ThreadPool {
    #[inline]
    pub fn new() -> Result<ThreadPool, std::io::Error> {
        ThreadPool::with_capacity(num_cpus::get())
    }

    pub fn with_capacity(capacity: usize) -> Result<ThreadPool, std::io::Error> {
        let base_name = String::from(type_name::<ThreadPool>());
        let default_stack_size: Option<NonZeroUsize> = None;

        let mut workers = Vec::with_capacity(capacity);
        let (tx, rx): (Sender<Box<dyn FnOnce() + Send + 'static>>, _) = std::sync::mpsc::channel();
        let rx = Arc::new(Mutex::new(rx));
        let len = Arc::new(AtomicUsize::new(0));

        for id in 1..=capacity {
            let mut name = base_name.clone();
            name.push_str(":");
            if let Some(id_str) = INT2STR.get(id) {
                name.push_str(id_str);
            } else {
                name.push_str(&id.to_string());
            }

            let mut builder = Builder::new().name(name);
            if let Some(size) = default_stack_size {
                builder = builder.stack_size(size.get());
            }

            let worker_rx = Arc::clone(&rx);
            let worker_len = Arc::clone(&len);

            match builder.spawn(move || loop {
                let job = worker_rx.lock().unwrap().recv();

                match job {
                    Ok(job) => {
                        println!("Worker {} running new job", id);
                        worker_len.fetch_sub(1, std::sync::atomic::Ordering::Release);
                        job();
                    }
                    Err(_) => {
                        println!("Worker {} shutting down", id);
                        break;
                    }
                };
            }) {
                Ok(worker) => workers.push(worker),
                Err(err) => {
                    return Err(err);
                }
            }
        }
        Ok(ThreadPool {
            workers,
            job_sender: tx,
            queue_len: len,
            base_name,
            default_stack_size,
        })
    }

    #[inline]
    pub fn push<F>(&mut self, f: F) -> Result<(), ()>
    where
        F: FnOnce() + Send + 'static,
    {
        let r = self.job_sender.send(Box::new(f)).map_err(|_| ());
        if r.is_ok() {
            self.queue_len
                .fetch_add(1, std::sync::atomic::Ordering::Release);
        }
        r
    }

    #[inline]
    pub fn queue_length(&self) -> usize {
        self.queue_len.load(std::sync::atomic::Ordering::SeqCst)
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.queue_length() == 0
    }

    #[inline]
    pub fn terminate(self) {
        let ThreadPool {
            workers,
            job_sender,
            ..
        } = self;
        drop(job_sender);

        for (id, handle) in workers.into_iter().enumerate() {
            println!("Waiting until worker {} has shut down", id + 1);
            handle.join().unwrap();
        }
    }
}

impl Default for ThreadPool {
    #[inline]
    fn default() -> Self {
        ThreadPool::new().unwrap()
    }
}

//! TODO

#![feature(allocator_api, error_in_core, io_error_other, strict_provenance)]
#![feature(result_option_inspect)]

#[cfg(feature = "alloc")]
extern crate alloc;

// mod http;
// mod http_stream;
// mod routing;
pub mod task;
mod threadpool;

use std::{
    collections::HashMap,
    pin::Pin,
    sync::mpsc::{Receiver, TryRecvError},
    task::{Context, Poll, RawWaker, RawWakerVTable, Waker},
};

use ::http::{Request, Uri};
use subcutaneous::Applicant;
pub use threadpool::ThreadPool;

pub struct Server {
    routes: HashMap<Uri, usize>,
    // applicants: Vec<Box<dyn Applicant<In = (), Out = (), Injector = StartedServer>>>,
}

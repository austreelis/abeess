//! TODO

use std::time::Duration;

use abeess_www::task::{Executor, TimerFuture};

fn main() {
    let (mut executor, spawner) = Executor::new();
    spawner.spawn(async {
        println!("shork");
        TimerFuture::new(Duration::new(3, 0)).await;
        println!("blahaaaaj");
    });
    spawner.spawn(async {
        println!("shork");
        TimerFuture::new(Duration::new(2, 0)).await;
        println!("blahaaaaj");
    });
    spawner.spawn(async {
        println!("shork");
        TimerFuture::new(Duration::new(1, 0)).await;
        println!("blahaaaaj");
    });
    drop(spawner);

    executor.run();
}
